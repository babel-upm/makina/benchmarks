defmodule Benchmarks.MixProject do
  use Mix.Project

  def project do
    [
      app: :benchmarks,
      version: "0.1.0",
      elixir: "~> 1.15",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:makina, git: "git@gitlab.com:babel-upm/makina/makina.git", branch: "develop"},
      {:benchee, "~> 1.2.0"},
      {:propcheck, "~> 1.4.1"}
    ]
  end
end
