defmodule BaseModel do
  import PropCheck.BasicTypes
  use Makina, implemented_by: Auth

  state users:  %{} :: %{ Auth.user() => Auth.pass() },
        tokens:  [] :: [symbolic(Auth.token())]

  invariants uniq_tokens: Enum.uniq(tokens) == tokens

  command reg(user :: Auth.user(), pass :: Auth.pass()) :: :ok | :error do
    args user: string(), pass: string()
    valid user not in Map.keys(users)
    next if valid, do: [users: Map.put(users, user, pass)]
    post if valid, do: result == :ok, else: result == :error
  end

  command gen(user :: Auth.user(), pass :: Auth.pass()) :: {:ok, Auth.token()} | :error do
    pre users != %{}
    args let user <- oneof(Map.keys(users)),
         do: [user: user, pass: Map.get(users, user)]
    valid {user, pass} in users
    next if valid, do: [tokens: [symbolic(Kernel.elem(result, 1))|tokens]]
    post if valid, do: match?({:ok, _}, result), else: result
  end

  command rev(token :: Auth.token()) :: :ok | :error do
    args token: oneof([pos_integer()|tokens])
    valid token in tokens
    next if valid, do: [tokens: List.delete(tokens, token)]
    post if valid, do: result == :ok, else: result == :error
  end

  command val(token :: Auth.token()) :: :ok | :error do
    args token: oneof([pos_integer()|tokens])
    valid token in tokens
    post if valid, do: result == :ok, else: result == :error
  end

  def string(), do: [char()]
end

defmodule Extends do
  import PropCheck.BasicTypes
  use Makina, extends: BaseModel,
              implemented_by: Docs,
              where: [val: :put, val: :del]

  state docs: %{} :: %{ Docs.key() => Docs.doc() }

  command put(key :: Docs.key(), doc :: Docs.doc()) :: :ok | :error do
    args key: integer(), doc: string()
    next if valid, do: [docs: Map.put(docs, key, doc)]
  end

  command del(key :: Docs.key()) :: :ok | :error do
    args key: oneof([integer()|Map.keys(docs)])
    valid key in Map.keys(docs)
    next if valid, do: [docs: Map.delete(docs, key)]
  end

  command get(key :: Docs.key()) :: {:ok, Docs.doc()} | :error do
    args key: oneof([integer()|Map.keys(docs)])
    post result == Map.fetch(docs, key)
  end

  def string(), do: [char()]
end
