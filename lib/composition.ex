defmodule Reg do
  import PropCheck.BasicTypes
  use Makina

  state users:  %{} :: %{ Auth.user() => Auth.pass() },
    tokens:  [] :: [symbolic(Auth.token())],
    docs: %{} :: %{ Docs.key() => Docs.doc() }

  command reg(user :: Auth.user(), pass :: Auth.pass()) :: :ok | :error do
    args user: string(), pass: string()
    call Auth.reg(user, pass)
    valid user not in Map.keys(users)
    next if valid, do: [users: Map.put(users, user, pass)]
    post if valid, do: result == :ok, else: result == :error
  end

  def string(), do: [char()]
end

defmodule Gen do
  import PropCheck.BasicTypes
  use Makina

  state users:  %{} :: %{ Auth.user() => Auth.pass() },
    tokens:  [] :: [symbolic(Auth.token())],
    docs: %{} :: %{ Docs.key() => Docs.doc() }

  command gen(user :: Auth.user(), pass :: Auth.pass()) :: {:ok, Auth.token()} | :error do
    pre users != %{}
    args let user <- oneof(Map.keys(users)),
      do: [user: user, pass: Map.get(users, user)]
    call Auth.gen(user, pass)
    valid {user, pass} in users
    next if valid, do: [tokens: [symbolic(Kernel.elem(result, 1))|tokens]]
    post if valid, do: match?({:ok, _}, result), else: result
  end

    def string(), do: [char()]
end

defmodule Rev do
  import PropCheck.BasicTypes
  use Makina

  state users:  %{} :: %{ Auth.user() => Auth.pass() },
    tokens:  [] :: [symbolic(Auth.token())],
    docs: %{} :: %{ Docs.key() => Docs.doc() }

  command rev(token :: Auth.token()) :: :ok | :error do
    args token: oneof([pos_integer()|tokens])
    call Auth.rev(token)
    valid token in tokens
    next if valid, do: [tokens: List.delete(tokens, token)]
    post if valid, do: result == :ok, else: result == :error
  end

  def string(), do: [char()]
end

defmodule Val do
  import PropCheck.BasicTypes
  use Makina

  state users:  %{} :: %{ Auth.user() => Auth.pass() },
    tokens:  [] :: [symbolic(Auth.token())],
    docs: %{} :: %{ Docs.key() => Docs.doc() }

  command val(token :: Auth.token()) :: :ok | :error do
    args token: oneof([pos_integer()|tokens])
    call Auth.val(token)
    valid token in tokens
    post if valid, do: result == :ok, else: result == :error
  end

    def string(), do: [char()]
end

defmodule Put do
  import PropCheck.BasicTypes
  use Makina

  state users:  %{} :: %{ Auth.user() => Auth.pass() },
    tokens:  [] :: [symbolic(Auth.token())],
    docs: %{} :: %{ Docs.key() => Docs.doc() }

  command put(token :: Auth.token(), key :: Docs.key(), doc :: Docs.doc()) :: :ok | :error do
    args token: oneof([pos_integer()|tokens]), key: integer(), doc: string()
    call Docs.put(token, key, doc)
    valid token in tokens
    next if valid, do: [docs: Map.put(docs, key, doc)]
  end
  def string(), do: [char()]
end

defmodule Del do
  import PropCheck.BasicTypes
  use Makina

  state users:  %{} :: %{ Auth.user() => Auth.pass() },
    tokens:  [] :: [symbolic(Auth.token())],
    docs: %{} :: %{ Docs.key() => Docs.doc() }

  command del(token :: Auth.token(), key :: Docs.key()) :: :ok | :error do
    args token: oneof([pos_integer()|tokens]), key: oneof([integer()|Map.keys(docs)])
    call Docs.del(token, key)
    valid key in Map.keys(docs) and token in tokens
    next if valid, do: [docs: Map.delete(docs, key)]
  end
  def string(), do: [char()]
end

defmodule Get do
  import PropCheck.BasicTypes
  use Makina

  state users:  %{} :: %{ Auth.user() => Auth.pass() },
    tokens:  [] :: [symbolic(Auth.token())],
    docs: %{} :: %{ Docs.key() => Docs.doc() }

  command get(token :: Auth.token(), key :: Docs.key()) :: {:ok, Docs.doc()} | :error do
    args token: oneof([pos_integer()|tokens]), key: oneof([integer()|Map.keys(docs)])
    call Docs.get(key)
    valid token in tokens
    post result == Map.fetch(docs, key)
  end
  def string(), do: [char()]
end

defmodule Composition do
  use Makina, extends: [Reg, Gen, Rev, Val, Put, Del, Get]

  invariants uniq_tokens: Enum.uniq(tokens) == tokens
end
