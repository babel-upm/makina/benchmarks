defmodule CompositionTest do
  require ExUnit.Assertions
  import ExUnit.Assertions
  use ExUnit.Case, async: false
  use PropCheck
  require PropCheck.StateM
  import PropCheck.StateM

  property "Composition makina state machine" do
    forall cmds <- commands(Composition) do
      Docs.start()

      r = {_, _, result} = run_commands(Composition, cmds)

      Docs.stop()

      (result == :ok)
      |> when_fail(print_report(r, cmds))
      |> aggregate(command_names(cmds))
    end
  end
end
