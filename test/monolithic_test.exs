defmodule MonolithicTest do
  require ExUnit.Assertions
  import ExUnit.Assertions
  use ExUnit.Case, async: false
  use PropCheck
  import PropCheck.StateM

  test "unit test" do
    assert :ok = Docs.start()
    assert :ok = Auth.reg("user", "password")
    assert {:ok, token} = Auth.gen("user", "password")
    assert :ok = Docs.put(token, 0, "text")
    assert {:ok, "text"} = Docs.get(0)
    assert :ok = Docs.del(token, 0)
    assert :error = Docs.get(0)
    assert :ok = Docs.stop()
  end

  property "makina state machine" do
    forall cmds <- commands(Monolithic) do
      Docs.start()

      r = {_, _, result} = run_commands(Monolithic, cmds)

      Docs.stop()

      (result == :ok)
      |> when_fail(print_report(r, cmds))
      |> aggregate(command_names(cmds))
    end
  end
end
