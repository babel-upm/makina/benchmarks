defmodule ExtendsTest do
  require ExUnit.Assertions
  import ExUnit.Assertions
  use ExUnit.Case, async: false
  use PropCheck
  require PropCheck.StateM
  import PropCheck.StateM

  test "unit test auth" do
    assert :ok = Auth.start()
    assert :ok = Auth.reg("user", "password")
    assert {:ok, token} = Auth.gen("user", "password")
    assert :ok = Auth.val(token)
    assert :ok = Auth.rev(token)
    assert :error = Auth.reg("user", "password")
    assert :error = Auth.val(token)
    assert :error = Auth.rev(token)
    assert :ok = Auth.stop()
  end

  test "unit test docs" do
    assert :ok = Docs.start()
    assert :ok = Auth.reg("user", "password")
    assert {:ok, token} = Auth.gen("user", "password")
    assert :ok = Docs.put(token, 0, "text")
    assert {:ok, "text"} = Docs.get(0)
    assert :ok = Docs.del(token, 0)
    assert :error = Docs.get(0)
    assert :ok = Docs.stop()
  end

  property "Auth makina state machine" do
    forall cmds <- commands(BaseModel) do
      assert :ok = Auth.start()

      r = {_, _, result} = run_commands(BaseModel, cmds)

      assert :ok = Auth.stop()

      (result == :ok)
      |> when_fail(print_report(r, cmds))
      |> aggregate(command_names(cmds))
    end
  end

  property "Docs makina state machine" do
    forall cmds <- commands(Extends) do
      Docs.start()

      r = {_, _, result} = run_commands(Extends, cmds)

      Docs.stop()

      (result == :ok)
      |> when_fail(print_report(r, cmds))
      |> aggregate(command_names(cmds))
    end
  end
end
