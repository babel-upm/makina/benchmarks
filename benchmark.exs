seed = :os.timestamp()
size = 10

# benchmark generating test cases of 1 command
Benchee.run(%{
      "monolithic"  => fn -> Monolithic.initial_state() |> Monolithic.command() |> PropCheck.produce(size, seed)  end,
      "extends"     => fn -> Extends.initial_state() |> Extends.command() |> PropCheck.produce(size, seed) end,
      "composition" => fn -> Composition.initial_state() |> Composition.command() |> PropCheck.produce(size, seed) end
            },
  warmup: 10, time: 20, parallel: 1)


# benchmark generating test cases of lengths 100, 1000 and 10000

inputs = for n <- [100, 1000, 10000] do
  {"size #{n}", n}
end |> Map.new()

gen_test_case = fn n, model ->
  for n <- 1..n, reduce: model.initial_state() do
    acc ->
      {:ok, command} = model.command(acc) |> PropCheck.produce()
      model.next_state(acc, {:var, n}, command)
  end
end

Benchee.run(%{
      "monolithic"  => fn size -> gen_test_case.(size, Monolithic) end,
      "extends"     => fn size -> gen_test_case.(size, Extends) end,
      "composition" => fn size -> gen_test_case.(size, Composition) end
            },
  inputs: inputs, warmup: 10, time: 20, parallel: 1)
